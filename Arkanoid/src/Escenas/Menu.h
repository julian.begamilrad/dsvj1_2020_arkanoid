#ifndef MENU_H
#define MENU_H
#include "raylib.h"

#include "Global.h"
namespace ARKANOIDJB
{
	static const int TEXTDISTANCEINX = 2;
	static const int PLAYTEXTDISTANCEINY = 6;
	static const int CONTROLESTEXTDISTANCEINY = 8;
	static const int CREDITSTEXTDISTANCEINY = 10;
	static const int EXITTEXTDISTANCEINY = 12;
	static const int DISTANCEDEFAULT = 30;

	class Menu
	{
		enum MenuOptions
		{
			Play, Controls, Credits, Exit
		};

	public:
		Menu();
		~Menu();
		void mInit();
		void mInput();
		void mUpdate();
		void mDraw();

	private:

		int actualOption;
		bool isControlMenu;

		Sound select;
		Sound moveSelection;
		Music music;
		bool musicOn;
		Texture2D menuBackGround;
	};

}
#endif
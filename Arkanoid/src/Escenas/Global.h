#ifndef GLOBAL_H
#define GLOBAL_H

#include "raylib.h"
namespace ARKANOIDJB
{
	const int TextSize = 30;
	const int  WIDTH = 80;
	const int HEIGHT= 20;

	const int QUARTERDIVIDER = 4;
	const int HALFDIVIDER = 2;
	const int THIRDDIVIDER = 3;
	const int INVERTMULTIPLIER = -1;
	const int DOUBLE = 2;
	const int TRIPLE = 3;
	const float PLAYERSPEED = 300.0f;
	const int PLAYERBASEHP = 3;
	const int POWERUPAPPEARTIME = 10;
	const int OFFSCREENPOS = -10;
	const int BASEPOINTSTOWIN = 2;
	const int PLAYERCOLORSCANT = 6;
	const int MAXBRICKHP = 5;
	const int POINTSPERHIT = 100;

	const int STARTDEFAULTNUMBER = 0;
	const int BRICKWIDTH = 90;
	const int BRICKHEIGHT = 22;
	const int BRICKDISTANCE = 10;

	const int TIMESECTION = 3;

	enum GameStage
	{
		MENU, GAME, CREDITS, EXIT
	};
	enum TIME
	{
		SECONDS, MINUTES, HOURS
	};

	class Global
	{
	public:
		 Global();
		 ~Global();
		static int getGamestatus();
		static int getLastGamestatus();

		static void setGamestatus(int ngstatus);
		static void setLastGamestatus(int nlgstatus);


		static int gamestatus;
		static int lastGamestatus;

	private:
	};

}
#endif
#include "Game.h"

namespace ARKANOIDJB
{
		Game::Game()
		{
			player.init();
			ball.init(player.getHalfPos());
			for (int i = 0; i < bricksCol; i++)
			{
				for (int j = 0; j < bricksRow; j++)
				{
					bricks[i][j].init(STARTDEFAULTNUMBER, STARTDEFAULTNUMBER);
				}				
			}
			inGame = true;
			pause = false;
			seconds = STARTDEFAULTNUMBER;
			timer[SECONDS] = STARTDEFAULTNUMBER;
			timer[MINUTES] = STARTDEFAULTNUMBER;
			timer[HOURS] = STARTDEFAULTNUMBER;
			music = LoadMusicStream("res/GameMusic.mp3");
			musicOn = true;
			allBricksBroken = false;
			lvl = STARTDEFAULTLVL;
			clearLvl = LoadSound("res/ClearLvl.ogg");
			noHP = LoadSound("res/NoHP.ogg");
			sPowerUp = LoadSound("res/PowerUp.ogg");
			hit = LoadSound("res/Hit.ogg");
			tryGetPowerUp = false;
			lastTryGetPowerUp = false;

			blueBrick = LoadTexture("res/Textures/Blue.png");
			orangeBrick = LoadTexture("res/Textures/Orange.png");
			greenBrick = LoadTexture("res/Textures/Green.png");
			pinkBrick = LoadTexture("res/Textures/Pink.png");
			purpleBrick = LoadTexture("res/Textures/Purple.png");

			playerTexture = LoadTexture("res/Textures/Player.png");
			ePlayerTexture = LoadTexture("res/Textures/ExtendedPlayer.png");
			ballTexture = LoadTexture("res/Textures/Ball.png");
			background = LoadTexture("res/Backgrounds/Game.png");
		}
		Game::~Game()
		{
			UnloadSound(clearLvl);
			UnloadSound(noHP);
			UnloadSound(sPowerUp);
			UnloadSound(hit);
			UnloadMusicStream(music);

			UnloadTexture(blueBrick);
			UnloadTexture(orangeBrick);
			UnloadTexture(greenBrick);
			UnloadTexture(pinkBrick);
			UnloadTexture(purpleBrick);
			UnloadTexture(playerTexture);
			UnloadTexture(ballTexture);
			UnloadTexture(background);
		}
		void Game::gInit()
		{
			player.init();
			ball.init(player.getHalfPos());
			for (int i = 0; i < bricksCol; i++)
			{
				for (int j = 0; j < bricksRow; j++)
				{
					bricks[i][j].init((i * BRICKWIDTH)+ BRICKDISTANCE *i+ BRICKDISTANCE, (j * BRICKHEIGHT) + BRICKDISTANCE *j + 80);
				}
			}
			inGame = true;
			pause = false;
			seconds = STARTDEFAULTNUMBER;
			timer[SECONDS] = STARTDEFAULTNUMBER;
			timer[MINUTES] = STARTDEFAULTNUMBER;
			timer[HOURS] = STARTDEFAULTNUMBER;
			allBricksBroken = false;
			lvl = STARTDEFAULTLVL;
			PlayMusicStream(music);
		}
		void Game::gInput()
		{
			ball.input();
			if (IsKeyReleased(KEY_P))
			{
				pause = !pause;

				if (pause) PauseMusicStream(music);
				else if (musicOn) ResumeMusicStream(music);
			}
			if (IsKeyReleased(KEY_N))
			{
				if (musicOn)PauseMusicStream(music);
				else if (!pause)ResumeMusicStream(music);
				musicOn = !musicOn;
			}
#if DEBUG
			if (IsKeyReleased(KEY_U))
			{
				PlaySound(clearLvl);
				lvl++;
			}
#endif
			if (pause != true)
			{
				player.input();				
			}
			if (pause == true)
			{
				if (IsKeyReleased(KEY_M))
				{
					Global::setGamestatus(MENU);
					gInit();
					pause = false;
				}

				if (IsKeyReleased(KEY_R))
				{
					gInit();
					pause = false;
				}
			}
		}
		void Game::gUpdate()
		{
			allBricksBroken = true;
			for (int i = 0; i < bricksCol; i++)
			{
				for (int j = 0; j < bricksRow; j++)
				{
					if (bricks[i][j].isAlive())
					{
						if (ball.checkColisionWihtBricks(bricks[i][j]))
						{
							bricks[i][j].setLives(bricks[i][j].getLives() - 1); // bricks lose 1 hp
							player.setPoints(player.getPoints() + POINTSPERHIT);
							player.setAlreadyHit(false);
							bricks[i][j].updateColor();
							PlaySound(hit);
							powerUp();
						}
						allBricksBroken = false;
					}
				}
			}
			if (allBricksBroken == true)
			{
				lvl++;
				ball.init(player.getHalfPos());
				PlaySound(clearLvl);
				player.setPSpeed(PLAYERSPEED);
				player.setPlayerWidth(WIDTH);
				for (int i = 0; i < bricksCol; i++)
				{
					for (int j = 0; j < bricksRow; j++) // j is the bricks row
					{
						int cantHP = lvl;
						if (lvl >= MAXBRICKHP) cantHP = MAXBRICKHP;
						bricks[i][j].setLives(1);
						if (j == 0) bricks[i][j].setLives(cantHP);
						if (j == 1)
						{
							if (cantHP > 1)
							bricks[i][j].setLives(cantHP -1);
						}
						if (j == 2)
						{
							if (cantHP > 2)
								bricks[i][j].setLives(cantHP - 2);
						}
						if (j == 3)
						{
							if (cantHP > 3)
								bricks[i][j].setLives(cantHP - 3);
						}
						if (j == 4 )
						{
							if (cantHP > 4)
								bricks[i][j].setLives(cantHP - 4);
						}						
						ball.init(player.getHalfPos());
						bricks[i][j].updateColor();
					}
				}
				
			}
			if (ball.checkHitGround())
			{
				player.setLives(player.getLives() - 1); // player lose 1 hp
				ball.init(player.getPosition());
				player.setAlreadyHit(false);
				player.setPSpeed(PLAYERSPEED);
				player.setPlayerWidth(WIDTH);
			}
			UpdateMusicStream(music);

			if (ball.checkHitRoof())
			{
				player.setAlreadyHit(false);
			}

			if (player.getLives() <= 0)
			{
				PlaySound(noHP);
				Global::setGamestatus(CREDITS);
				Credits::setPoints(player.getPoints());
				Credits::setLvl(lvl);
				Credits::setTime(timer[HOURS], timer[MINUTES], timer[SECONDS]);
				gInit();
				pause = false;
			}
		
			if (pause != true)
			{
				gameTime();
				ball.update(player.getHalfPos());
				if (ball.checkColisionWihtPlayer(player))
				{
					player.setAlreadyHit(true);
				}
				
			}
		}
		void Game::gDraw()
		{
			Vector2 backPos = {0,0 };
			DrawTextureEx(background, backPos, 0, 0.4, WHITE);
			ball.drawMe();
			player.drawMe();
			Vector2 pPos = { player.getBody().x  , player.getBody().y };
			if (player.getBody().width <= WIDTH)
			{
				DrawTextureEx(playerTexture, pPos, 0, 0.044, WHITE);
			}
			else
			{
				DrawTextureEx(ePlayerTexture, pPos, 0, 0.044, WHITE);
			}
			
			Vector2 bPos = { ball.ballGetPosition().x -12 , ball.ballGetPosition().y - 13 };
			DrawTextureEx(ballTexture, bPos, 0, 0.033, WHITE);
			DrawText(TextFormat("TIME %1i", timer[MINUTES]), GetScreenWidth() / HALFDIVIDER - MeasureText("TIME 100 :", 40) / HALFDIVIDER, GetScreenHeight() / 8 - 25, 25, GRAY);
			DrawText(TextFormat(": %1i", timer[SECONDS]), GetScreenWidth() / HALFDIVIDER - MeasureText(".", 40) / 2, GetScreenHeight() / 8 - 25, 25, GRAY);
			DrawText(TextFormat("lvl %1i", lvl), GetScreenWidth() - GetScreenWidth() / 5, (GetScreenHeight() / 8) - (GetScreenHeight() /18) , 25, GRAY);

			for (int i = 0; i < bricksCol; i++)
			{
				for (int j = 0; j < bricksRow; j++)
				{
					if (bricks[i][j].isAlive()) 
					{
						bricks[i][j].updateColor();
						Vector2 pos = { bricks[i][j].getBody().x  , bricks[i][j].getBody().y };
						Color comparColor = bricks[i][j].getColor();
					if (comparColor.a == PURPLE.a && comparColor.r == PURPLE.r && comparColor.g == PURPLE.g && comparColor.b == PURPLE.b) DrawTextureEx(purpleBrick, pos, 0, 0.05, WHITE);
					if (comparColor.a == BLUE.a && comparColor.r == BLUE.r && comparColor.g == BLUE.g && comparColor.b == BLUE.b) DrawTextureEx(blueBrick, pos, 0, 0.05, WHITE);
					if (comparColor.a == GREEN.a && comparColor.r == GREEN.r && comparColor.g == GREEN.g && comparColor.b == GREEN.b) DrawTextureEx(greenBrick, pos, 0, 0.05, WHITE);
					if (comparColor.a == PINK.a && comparColor.r == PINK.r && comparColor.g == PINK.g && comparColor.b == PINK.b) DrawTextureEx(pinkBrick, pos, 0, 0.05, WHITE);
					if (comparColor.a == ORANGE.a && comparColor.r == ORANGE.r && comparColor.g == ORANGE.g && comparColor.b == ORANGE.b) DrawTextureEx(orangeBrick, pos, 0, 0.05, WHITE);				
					}
				}
			}

			if (pause == true)
			{
				DrawText("GAME PAUSED", GetScreenWidth() / 2 - MeasureText("GAME PAUSED", 40) / HALFDIVIDER, GetScreenHeight() / HALFDIVIDER - 40, 40, YELLOW);
				DrawText("M to go back to menu, R to reset the game", GetScreenWidth() / HALFDIVIDER - MeasureText("M to go back to menu, R to reset the game", 20) / 2, GetScreenHeight() / 2, 20, YELLOW);
			}
			if (ball.getIfLaunched() == false)
			{
				DrawText("Press space to launch the ball", GetScreenWidth() / HALFDIVIDER - MeasureText("Press space to launch the ball", TextSize) / HALFDIVIDER, (GetScreenHeight() / HALFDIVIDER) + (TextSize * DOUBLE), TextSize, LIGHTGRAY);
			}
		}
		void Game::gameTime()
		{
			if (pause != true)
				seconds += GetFrameTime();
			if (seconds >= 1) {
				timer[SECONDS]++;
				if (timer[SECONDS] >= 60) {
					timer[SECONDS] = 0;
					timer[MINUTES]++;
				}
				if (timer[MINUTES] >= 60) {
					timer[MINUTES] = 0;
					timer[HOURS]++;
				}
				seconds = 0;
			}
		}
		void Game::powerUp()
		{
			lastTryGetPowerUp = tryGetPowerUp;
			tryGetPowerUp = GetRandomValue(0, 50);
			if (tryGetPowerUp == lastTryGetPowerUp)
			{
				
				tryGetPowerUp = GetRandomValue(0, 50);
			}
			bool getPowerUp = false;
			if (tryGetPowerUp <= 1)
			{
				PlaySound(sPowerUp);
				getPowerUp = true;
				cout << "get power Up" << endl;
			}
			if (getPowerUp == true)
			{
				int whichPowerUp = GetRandomValue(0, 15);
				cout << whichPowerUp << endl;
				switch (whichPowerUp)
				{
				case LIFEUP:
					player.setLives(player.getLives() + 1);
					cout << "Life Up" << endl;
					break;
				case GROWPADDLE :					
					player.setPlayerWidth(WIDTH + (WIDTH/2) );
					cout << "Paddle Up" << endl;
					break;
				case GROWPADDLE2:
					player.setPlayerWidth(WIDTH + (WIDTH / 2));
					cout << "Paddle Up" << endl;
					break;
				case PADDLESPEED:
					player.setPSpeed(PLAYERSPEED + (PLAYERSPEED / 2));
					cout << "Speed Up" << endl;
					break;
				case PADDLESPEED2:
					player.setPSpeed(PLAYERSPEED + (PLAYERSPEED / 2));
					cout << "Speed Up" << endl;
					break;
				default:
					player.setPoints(player.getPoints() + 1000);
					cout << "Points Up" << endl;
					break;
				}
			}
		}
}

#ifndef BRICK_H
#define BRICK_H
#include "raylib.h"
#include "Escenas/Global.h"
namespace ARKANOIDJB
{

	const int INITHP = 1;
	class Brick
	{
	public:
		Brick();
		~Brick();
		void init(int posX, int posY);
		void drawMe();

		bool isAlive();
		Rectangle getBody();
		Color getColor();
		int getLives();
		void updateColor();
		bool getUnbreackable();
		bool getNullBrick();
		
		void setPosition(Vector2 nPos);
		void setLives(int nLives);
		void setColor(Color nColor);
		void setNullBrick(bool nBrick);
		void setUnbreackable(bool uBrick);

		
	private:
		bool nullBrick;
		bool unbreackable;
		int lives;
		Color myColor;
		Rectangle myBody;


	};

}
#endif
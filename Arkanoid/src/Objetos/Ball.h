#ifndef BALL_H
#define BALL_H

#include "raylib.h"

#include "Brick.h"
#include "Player.h"

namespace ARKANOIDJB
{
	const float XLONGANGLESPEED = 300.0f;
	const float XSHORTANGLESPEED = 150.0f;
	const float YLONGANGLESPEED = 100.0f;
	const float YSHORTANGLESPEED = 250.0f;
	const int BALLSPEED = 5;
	const int RADIUS = 12;
	class Ball
	{
	public:
		Ball();
		~Ball();
		void init(Vector2 paddlePos);
		void update(Vector2 paddlePos);
		void input();
		void drawMe();
		void launch();
		void move();
		void testLimits();
		bool checkColisionWihtPlayer(Player Player1);
		bool checkColisionWihtBricks(Brick block);
		bool checkHitGround();
		bool checkHitRoof();

		Vector2 getSpeed();
		float getRadius();
		bool getIfLaunched();
		Vector2 ballGetPosition();

		void setSpeed(Vector2 nSpeed);
		void setPosition(Vector2 newPos);

	private:

		
		bool launched;
		Vector2 position;
		float radius;
		Color myColor;
		Vector2 speed;

		

	};
}

#endif
#include "Brick.h"
namespace ARKANOIDJB
{
	Brick::Brick()
	{
		 lives = INITHP;
		 myColor = PURPLE;
		 myBody.x = STARTDEFAULTNUMBER;
		 myBody.y = STARTDEFAULTNUMBER;
		 myBody.height = BRICKHEIGHT;
		 myBody.width = BRICKWIDTH;
		 nullBrick = false;
		 unbreackable = false;

	}
	Brick::~Brick()
	{

	}
	void Brick::init(int posX, int posY)
	{
		lives = INITHP;
		myColor = PURPLE;
		myBody.x = posX;
		myBody.y = posY;
		myBody.height = BRICKHEIGHT;
		myBody.width = BRICKWIDTH;
		nullBrick = false;
		unbreackable = false;
	}
	void Brick::drawMe()
	{
#if DEBUG
		DrawRectangle(myBody.x - 2, myBody.y - 2, myBody.width + 4, myBody.height + 4, RED);
#endif
		DrawRectangle(myBody.x, myBody.y, myBody.width, myBody.height, myColor);
		
	}

	bool Brick::isAlive()
	{
		if (lives >= 1) 
		{
			return true; 
		}
		else return false;
		
	}
	Rectangle Brick::getBody()
	{
		return myBody;
	}

	Color Brick::getColor()
	{
		switch (lives)
		{
		case 1:
			return PURPLE;
			break;
		case 2:
			return PINK;
			break;
		case 3:
			return BLUE;
			break;
		case 4:
			return GREEN;
			break;
		case 5:
			return ORANGE;
			break;
		default:
			break;
		}
	}
	int Brick::getLives()
	{
		return lives;
	}
	void Brick::updateColor()
	{
		switch (lives)
		{
		case 1:
			setColor(PURPLE);
			break;
		case 2:
			setColor(PINK);
			break;
		case 3:
			setColor(BLUE);
			break;
		case 4:
			setColor(GREEN);
			break;
		case 5:
			setColor(ORANGE);
			break;
		case 6:
			setColor(YELLOW);
			break;
		default:
			break;
		}
	}
	bool Brick::getUnbreackable()
	{
		return unbreackable;
	}
	bool Brick::getNullBrick()
	{
		return nullBrick;
	}
	void Brick::setPosition(Vector2 nPos)
	{
		myBody.x = nPos.x;
		myBody.y = nPos.y;
	}
	void Brick::setLives(int nLives)
	{
		lives = nLives;
	}
	void Brick::setColor(Color nColor)
	{
		myColor = nColor;
	}
	void Brick::setNullBrick(bool nBrick)
	{
		nullBrick = nBrick;
	}
	void Brick::setUnbreackable(bool uBrick)
	{
		unbreackable = uBrick;
	}
}

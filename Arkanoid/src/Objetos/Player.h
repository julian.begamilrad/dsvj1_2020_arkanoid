#ifndef PLAYER_H
#define PLAYER_H
#include "raylib.h"
#include "Escenas/Global.h"
namespace ARKANOIDJB
{
	class Player
	{
	public:
		Player();
		~Player();
		void init();
		void input();
		void drawMe();

		Rectangle getBody();
		Vector2 getPosition();
		Vector2 getHalfPos();
		int getLives();
		int getPoints();
		bool getAlreadyHit();
		float getPSpeed();

		void moveLeft();
		void moveRigth();

		void setAlreadyHit(bool dohit);
		void setPosition(Vector2 NewPosition);
		void setPoints(int npoints);
		void setLives(int nLives);
		void setPSpeed(float nSpeed);
		void setPlayerWidth(int nWidth);
	private:
		bool alreadyHit;
		int points;
		int lives;
		Rectangle myBody;
		Color myColor;
		float pSpeed;
		//Texture2D myTexture;
	};
}
#endif

#include "Ball.h"
namespace ARKANOIDJB
{
	Ball::Ball()
	{
		float posX = GetScreenWidth() / HALFDIVIDER;
		float posY = GetScreenHeight() / HALFDIVIDER;
		position = { posX,posY };
		radius = RADIUS;
		myColor = WHITE;
		speed = { STARTDEFAULTNUMBER,STARTDEFAULTNUMBER };
		launched = false;
	}

	Ball::~Ball()
	{
	}

	void Ball::init(Vector2 paddlePos)
	{
		position = { paddlePos.x, paddlePos.y-radius };
		radius = RADIUS;
		myColor = WHITE;
		speed = { STARTDEFAULTNUMBER,STARTDEFAULTNUMBER };
		launched = false;
	}

	void Ball::update(Vector2 paddlePos)
	{
		if (!launched)
			position = { paddlePos.x, paddlePos.y - radius };

		move();
		testLimits();
	}

	void Ball::input()
	{
		if (IsKeyReleased(KEY_SPACE))
		{
			if (launched == false)
			{
				launch();
			}
		}

	}

	void Ball::drawMe()
	{
		DrawCircleV(position, radius, myColor);
	}

	void Ball::launch()
	{
		launched = true;
		int start = GetRandomValue(0, 3);
		switch (start)
		{
		case 0:
			speed = { -XLONGANGLESPEED, -YLONGANGLESPEED };
			break;
		case 1:
			speed = { -XSHORTANGLESPEED, -YSHORTANGLESPEED };
			break;
		case 2:
			speed = { XLONGANGLESPEED, -YLONGANGLESPEED };
			break;
		case 3:
			speed = { XSHORTANGLESPEED, -YSHORTANGLESPEED };
			break;

		default:
			break;
		}
	}

	void Ball::move()
	{
		if (launched)
		{
			position.x = position.x  + ( speed.x * GetFrameTime());
			position.y = position.y + (speed.y * GetFrameTime());
		}
	}

	void Ball::testLimits()
	{
		if (position.x - radius <= 0)
		{
			if (speed.x < 0) setSpeed({ speed.x * (-1), speed.y });
		}
		if (position.x + radius >= GetScreenWidth())
		{
			if (speed.x >  0) setSpeed({ speed.x * (-1), speed.y });
		}
		if (position.y - radius <= 0) setSpeed({ speed.x, speed.y * (-1) });
		if (position.y + radius >= GetScreenHeight()) setSpeed({ speed.x, speed.y * (-1) });
	}

	bool Ball::checkColisionWihtPlayer(Player Player1)
	{
		if (CheckCollisionCircleRec(position, radius, Player1.getBody()) && Player1.getAlreadyHit() == false)
		{
			if (position.x < Player1.getHalfPos().x)
			{
				float Quarter = Player1.getBody().x + ((Player1.getHalfPos().x - Player1.getBody().x) / HALFDIVIDER);
				if (position.x < Quarter )
				{
					setSpeed({ -XLONGANGLESPEED, -YLONGANGLESPEED });
				}
				else 
				{
					setSpeed({ -XSHORTANGLESPEED, -YSHORTANGLESPEED });
				}
			}
			else 
			{
				float tQuarter = Player1.getHalfPos().x + ((Player1.getHalfPos().x - Player1.getBody().x) / HALFDIVIDER);
				if (position.x > tQuarter)
				{
					setSpeed({ XLONGANGLESPEED, -YLONGANGLESPEED });
				}
				else
				{
					setSpeed({ XSHORTANGLESPEED, -YSHORTANGLESPEED });
				}
			}


			return true;
		}
		else return false;

	}

	bool Ball::checkColisionWihtBricks(Brick block)
	{
		if (CheckCollisionCircleRec(position, radius, block.getBody()))
		{
			if (block.isAlive())
			{
				if (position.x < block.getBody().x || position.x >(block.getBody().x + block.getBody().width))
				{
					
					if (position.y >(block.getBody().y + block.getBody().height) || position.y < block.getBody().y)
					{
						if (speed.x > 0 && position.x > (block.getBody().x + block.getBody().width))
						{
							setSpeed({ speed.x, speed.y * (-1) });
						}
						else if (speed.x < 0 && position.x > (block.getBody().x + block.getBody().width))
						{
							setSpeed({ speed.x * (-1), speed.y  });
						}
						else if (speed.x < 0 && position.x < block.getBody().x)
						{
							setSpeed({ speed.x, speed.y * (-1) });
						}
						else if (speed.x > 0 && position.x < block.getBody().x)
						{
							setSpeed({ speed.x * (-1), speed.y});
						}
						else
						{
							setSpeed({ speed.x * (-1), speed.y * (-1) });
						}
					}
					else
					{
						setSpeed({ speed.x * (-1), speed.y });
					}
				}
				else
				{
					setSpeed({ speed.x, speed.y * (-1) });
				}
			
			return true;
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}

	bool Ball::checkHitGround()
	{
		if (position.y + radius >= GetScreenHeight())
		{
			return true;
		}
		else return false;
	}

	bool Ball::checkHitRoof()
	{
		if (position.y - radius <= 0)
		{
			return true;
		}
		else return false;
	}

	Vector2 Ball::getSpeed()
	{
		return speed;
	}

	float Ball::getRadius()
	{
		return radius;
	}

	bool Ball::getIfLaunched()
	{
		return launched;
	}

	Vector2 Ball::ballGetPosition()
	{
		return {position.x, position.y};
	}

	void Ball::setSpeed(Vector2 nSpeed)
	{
		speed = nSpeed;
	}

	void Ball::setPosition(Vector2 newPos)
	{
		position = newPos;
	}
}


#include "Player.h"
namespace ARKANOIDJB
{
	ARKANOIDJB::Player::Player()
	{
		pSpeed = STARTDEFAULTNUMBER;
		myBody.height = STARTDEFAULTNUMBER;
		myBody.width = STARTDEFAULTNUMBER;
		myBody.y = STARTDEFAULTNUMBER;
		myBody.x = STARTDEFAULTNUMBER;
		myColor = WHITE;
		lives = PLAYERBASEHP;
		points = STARTDEFAULTNUMBER;
		alreadyHit = false;
		//myTexture = LoadTexture("res/Textures/Player.png");
	}
	Player::~Player()
	{
		//UnloadTexture(myTexture);
	}
	void Player::init()
	{
		pSpeed = PLAYERSPEED;
		myBody.height = HEIGHT;
		myBody.width = WIDTH;
		myBody.y = GetScreenHeight() - HEIGHT*DOUBLE;
		myBody.x = (GetScreenWidth()/ HALFDIVIDER) - (WIDTH / HALFDIVIDER);
		lives = PLAYERBASEHP;
		points = STARTDEFAULTNUMBER;
		alreadyHit = false;
	}
	void Player::input()
	{
		if (IsKeyDown(KEY_A) && this->myBody.x >= 0)
		{
			moveLeft();
		}
		if (IsKeyDown(KEY_D) && this->myBody.x <= (GetScreenWidth() - this->myBody.width))
		{
			moveRigth();
		}

	}

	void Player::drawMe()
	{
		/*Vector2 pos = { myBody.x, myBody.y };
		DrawRectangle(static_cast<int>(myBody.x), myBody.y, myBody.width, myBody.height, myColor);
		DrawTextureEx(myTexture, pos, 0, 0.05, WHITE);*/ // por alguna razon la textura funciona si la llamo en game pero no si la llamo desde ac�
		DrawText(TextFormat("Lives %1i", lives), 100, GetScreenHeight() / 8 - 25, 25, GRAY);
		DrawText(TextFormat("Points %1i", points), GetScreenWidth() - GetScreenWidth() / 2.5, GetScreenHeight() / 8 - 25, 25, GRAY);
	}

	Rectangle Player::getBody()
	{
		return myBody;
	}
	Vector2 Player::getPosition()
	{
		return { getBody().x, getBody().y };
	}

	Vector2 Player::getHalfPos()
	{
		return { getBody().x + getBody().width / 2, getBody().y };
	}

	int Player::getLives()
	{
		return lives;
	}

	int Player::getPoints()
	{
		return points;
	}

	bool Player::getAlreadyHit()
	{
		return alreadyHit;
	}

	float Player::getPSpeed()
	{
		return pSpeed;
	}

	void Player::moveLeft()
	{
		Vector2 NewPosition = getPosition();
		NewPosition.x = NewPosition.x - (pSpeed * GetFrameTime());
		this->setPosition(NewPosition);
	}
	void Player::moveRigth()
	{
		Vector2 NewPosition = getPosition();
		NewPosition.x = NewPosition.x + (pSpeed * GetFrameTime());
		this->setPosition(NewPosition);
	}
	void Player::setAlreadyHit(bool dohit)
	{
		alreadyHit = dohit;
	}
	void Player::setPosition(Vector2 NewPosition)
	{
		myBody.x = NewPosition.x;
		myBody.y = NewPosition.y;
	}
	void Player::setPoints(int npoints)
	{
		points = npoints;
	}
	void Player::setLives(int nLives)
	{
		lives = nLives;
	}
	void Player::setPSpeed(float nSpeed)
	{
		pSpeed = nSpeed;
	}
	void Player::setPlayerWidth(int nWidth)
	{
		myBody.width = nWidth;
	}
}